//
//  HomeView.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/23/23.
//

import SwiftUI
import Kingfisher

struct HomeView: View {
    @ObservedObject var platformViewModel: PlatformViewModel
    @State var isGlowing = false
    @State var isShowingGenre = false
    @State var isShowingBSS = false
    
    @FocusState private var readyToType: Bool

    private let gridColumns = [
        GridItem(.adaptive(minimum: 300))
    ]
    
    init(platformViewModel: PlatformViewModel) {
        self.platformViewModel = platformViewModel
    }
    var body: some View {
        
        
        GeometryReader { gameGeometry in
            
            VStack {
                HStack {
                    Button {
//                        platformViewModel.fetchAllGames()
                        isShowingBSS.toggle()
                        readyToType = true
                    } label: {
                        Image(systemName: "1.magnifyingglass")
                            .font(.system(size: 24))
                            .foregroundColor(.red)
                            .overlay {
                                Image(systemName: "1.magnifyingglass")
                                    .font(.system(size: 24))
                                    .foregroundColor(.red)
                                    .shadow(color: .white, radius: 10, x: 0, y: 0)

                            }
                    }
                    .sheet(isPresented: $isShowingBSS) {
                        BSSView(platformViewModel: platformViewModel, isShowingGameInfoPage: false)
                    }

                    Text("Choose your platform")
                        .foregroundColor(.black)
                        .font(.system(size: 32))
                        .overlay(
                            Text("Choose your platform")
                                .foregroundColor(.red)
                                .font(.system(size: 32))
                                .opacity(isGlowing ? 1 : 0)
                                .shadow(color: .white, radius: 10, x: 0, y: 0)
                                .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true), value: isGlowing)
                        )
                        .onAppear{
                            self.isGlowing = true
                        }
                }
                ScrollView {
                    LazyVGrid(columns: gridColumns, spacing: 24) {
                        ForEach(platformViewModel.platforms, id: \.self) { platform in
                            VStack {
                                
//                                switch platformViewModel.state {
//                                case .idle:
//                                    ProgressView()
//                                    break
//                                if platformViewModel.state == .fetching {
//                                    ProgressView()
//                                }
//                                    break
//                                case .done:
                                if platformViewModel.state == .done {
                                    Button {
                                        //code to fetch platform games on next page
                                        platformViewModel.fetchPlatformDetail(result: platform)
                                        platformViewModel.fetchGenres()
                                        isShowingGenre = true
                                    } label: {
                                        VStack {
                                            KFImage(URL(string: platform.imageBackground))
                                                .placeholder {
                                                    
                                                    Image(systemName: "gamecontroller")
                                                        .resizable()
                                                        .frame(width: gameGeometry.size.width / 5, height: gameGeometry.size.height / 5 )
                                                }
                                                .resizable()
                                                .scaledToFit()
                                                .clipShape(RoundedRectangle(cornerRadius: 16))
                                                .padding(8)
                                            
                                            Text(platform.name)
                                                .foregroundColor(.red)
                                                .font(.system(size: 24))
                                                .overlay(
                                                    Text(platform.name)
                                                        .foregroundColor(.red)
                                                        .font(.system(size: 24))
                                                        .opacity(1)
                                                        .shadow(color: .white, radius: 10, x: 0, y: 0)
                                                    
                                                )
                                                .onAppear{
                                                    self.isGlowing = true
                                                }
                                            
                                        }
                                    }
                                }
                                
//                                if platformViewModel.state == .error {
//
//                                }
//                                    break
//
//                                case .error:
//
//                                    break
//                                }
                                
//                                Divider()
//                                    .background(.white)
//                                    .padding(0)
                            
                            }
                        }
                        .onAppear {
                            print("Item appeared!")
                        }
                    }
                }
            }
            .frame(width: gameGeometry.size.width, height: gameGeometry.size.height)
            .background(.black)
            .navigationDestination(isPresented: $isShowingGenre) {
                GenreView(platformViewModel: platformViewModel)
            }
        }        
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(platformViewModel: PlatformViewModel.mock())
    }
}
