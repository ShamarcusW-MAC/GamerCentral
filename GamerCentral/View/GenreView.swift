//
//  PlatformView.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/24/23.
//

import SwiftUI
import Kingfisher

struct GenreView: View {
    
    @ObservedObject var platformViewModel: PlatformViewModel
    @State var isGlowing = false
    @State var isShowingGames = false

    private let gridColumns = [
        GridItem(.adaptive(minimum: 150))
    ]
    
    //    init(platformViewModel: PlatformViewModel) {
    //        self.platformViewModel = platformViewModel
    //    }
    //
    var body: some View {
        
        GeometryReader { gameGeometry in
            
            VStack {
                Text("Choose Your Genre")
                    .foregroundColor(.black)
                    .font(.system(size: 32))
                    .overlay(
                        Text("Choose Your Genre")
                            .foregroundColor(.red)
                            .font(.system(size: 32))
                            .opacity(isGlowing ? 1 : 0)
                            .shadow(color: .white, radius: 10, x: 0, y: 0)
                            .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true), value: isGlowing)
                    )
                    .onAppear{
                        self.isGlowing = true
                    }
                
                Text("For")
                    .foregroundColor(.black)
                    .font(.system(size: 32))
                    .overlay(
                        Text("For")
                            .foregroundColor(.red)
                            .font(.system(size: 32))
                            .opacity(isGlowing ? 1 : 0)
                            .shadow(color: .white, radius: 10, x: 0, y: 0)
                            .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true), value: isGlowing)
                    )
                    .onAppear{
                        self.isGlowing = true
                    }
                
                VStack {
                    KFImage(URL(string: platformViewModel.chosenPlatform?.imageBackground ?? ""))
                        .placeholder {
                            Image(systemName: "gamecontroller")
                                .resizable()
                                .frame(width: gameGeometry.size.width / 5, height: gameGeometry.size.height / 5 )
                        }
                        .resizable()
                        .scaledToFit()
                        .clipShape(RoundedRectangle(cornerRadius: 16))
                        .padding(8)
                    
                    Text(platformViewModel.chosenPlatform?.name ?? "")
                        .foregroundColor(.red)
                        .font(.system(size: 24))
                        .overlay(
                            Text(platformViewModel.chosenPlatform?.name ?? "")
                            .foregroundColor(.red)
                            .font(.system(size: 24))
                            .opacity(1)
                            .shadow(color: .white, radius: 10, x: 0, y: 0)

                        )
                        .onAppear{
                            self.isGlowing = true
                        }
                    
                }
                
                Divider()
                    .background(.white)

                ScrollView(.horizontal) {
                    LazyHStack {
                    ForEach(platformViewModel.genres, id: \.self) { genre in
                       
                        Button {
                            platformViewModel.fetchGenreDetail(genre: genre)
                            platformViewModel.fetchPlatformAndGenreGames(platformId: platformViewModel.chosenPlatform?.id ?? 0, genreId: genre.id)
                            isShowingGames = true
                        } label: {
                            VStack {
                                KFImage(URL(string: genre.imageBackground))
                                    .placeholder {
                                        
                                        Image(systemName: "gamecontroller")
                                            .resizable()
                                            .frame(width: gameGeometry.size.width / 5, height: gameGeometry.size.height / 5 )
                                    }
                                    .resizable()
                                    .frame(width: gameGeometry.size.width / 2 )
                                    .scaledToFit()
                                    .clipShape(RoundedRectangle(cornerRadius: 16))
                                    .padding(32)
                                
                                Text(genre.name)
                                    .foregroundColor(.red)
                                    .font(.system(size: 24))
                                
                            }
                        }
                        
                        Divider()
                            .background(.white)
                        }
                    }
                }
                .frame(width: gameGeometry.size.width, height: gameGeometry.size.height / 3)
                
                Divider()
                    .background(.white)
                
                Spacer()

            }
            .background(.black)
            .navigationDestination(isPresented: $isShowingGames) {
                GamesView(platformViewModel: platformViewModel)
            }
        }
//        .toolbar(.hidden)
    }
}

struct GenreView_Previews: PreviewProvider {
    static var previews: some View {
        GenreView(platformViewModel: PlatformViewModel.mock())
    }
}
