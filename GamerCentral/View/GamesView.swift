//
//  GamesView.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/25/23.
//

import SwiftUI
import Kingfisher

struct GamesView: View {
    
    @ObservedObject var platformViewModel: PlatformViewModel
    @State var isGlowing = false
    @State var isShowingGameInfo = false

    var body: some View {
        
        GeometryReader { gameGeometry in
            VStack {
                Text("Platform")
                    .foregroundColor(.red)
                    .font(.system(size: 24))
                
                KFImage(URL(string: platformViewModel.chosenPlatform?.imageBackground ?? ""))
                    .placeholder {
                        Image(systemName: "gamecontroller")
                            .resizable()
                            .frame(width: gameGeometry.size.width / 5, height: gameGeometry.size.height / 5 )
                    }
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 16))
                    .padding(8)
                
                Text(platformViewModel.chosenPlatform?.name ?? "")
                    .foregroundColor(.red)
                    .font(.system(size: 24))
                
                Divider()
                    .background(.white)
                
                Text("Genre")
                    .foregroundColor(.red)
                    .font(.system(size: 24))
                
                KFImage(URL(string: platformViewModel.chosenGenre?.imageBackground ?? ""))
                    .placeholder {
                        Image(systemName: "gamecontroller")
                            .resizable()
                            .frame(width: gameGeometry.size.width / 5, height: gameGeometry.size.height / 5 )
                    }
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 16))
                    .padding(8)
                
                Text(platformViewModel.chosenGenre?.name ?? "")
                    .foregroundColor(.red)
                    .font(.system(size: 24))
                
                
                Divider()
                    .background(.white)
                                
                Text("Below are a few video games to choose from")
                    .foregroundColor(.red)
                    .font(.system(size: 18))
                    .padding(8)
                
                
                ScrollView(.horizontal) {
                    LazyHStack {
                        
                        if(platformViewModel.filteredGames.isEmpty) {
                            
                            VStack {
                                
                                Image(systemName: "figure.fall")
                                    .font(.system(size: 64))
                                    .foregroundColor(.red)
                                    .padding(.bottom, 32)
                                

                                Text("Sorry, couldn't find any games under that genre and/or platform.")
                                    .frame(width: gameGeometry.size.width)
                                    .fixedSize(horizontal: false, vertical: true)
                                    .multilineTextAlignment(.center)
                                    .lineLimit(3)
                                    .foregroundColor(.red)
                                    .font(.system(size: 24))
                            }
                            
                        } else {
                            ForEach(platformViewModel.filteredGames, id: \.self) { game in
                                
                                Button {
                                    platformViewModel.fetchGameInfo(id: game.id, filteredGame: game)
                                    isShowingGameInfo = true

                                } label: {
                                    VStack {
                                        KFImage(URL(string: game.backgroundImage ?? ""))
                                            .placeholder {
                                                
                                                Image(systemName: "gamecontroller")
                                                    .resizable()
                                                    .frame(width: gameGeometry.size.width / 5, height: gameGeometry.size.height / 5 )
                                            }
                                            .resizable()
                                            .frame(width: gameGeometry.size.width / 2 )
                                            .scaledToFit()
                                            .clipShape(RoundedRectangle(cornerRadius: 16))
                                            .padding(32)
                                        
                                        Text(game.name)
                                            .foregroundColor(.red)
                                            .font(.system(size: 16))
                                        
                                    }
                                }
                                Divider()
                                    .background(.white)
                            }
                            
                        }
                        
                    }
                }
                .frame(width: gameGeometry.size.width, height: gameGeometry.size.height / 3)
                
                
            }
            .background(.black)
            .navigationDestination(isPresented: $isShowingGameInfo) {
                GameInfoView(platformViewModel: platformViewModel)
            }
        }
    }
}

struct GamesView_Previews: PreviewProvider {
    static var previews: some View {
        GamesView(platformViewModel: PlatformViewModel.mock())
    }
}
