//
//  GCLoginView.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/27/23.
//

import SwiftUI

struct GCLoginView: View {
    var body: some View {
        GeometryReader { gameGeometry in
            VStack {
             
                HStack {
                    
                    Image(systemName: "gamecontroller")
                        .frame(width: gameGeometry.size.width / 6)
                        .scaledToFit()
                        .foregroundColor(.yellow)
                    
                    Text("Gamer Central")
                        .multilineTextAlignment(.leading)
                        .frame(maxWidth: .infinity)
                        .font(.system(size: 36))
                        .foregroundColor(.red)
                        .fontWeight(.bold)
                    
                    Image(systemName: "gamecontroller")
                        .frame(width: gameGeometry.size.width / 6)
                        .foregroundColor(.yellow)
                    
                }
                .padding(.top, 32)
                
                Spacer()
                
                HStack {
                    Image(systemName: "envelope")
                        .frame(width: gameGeometry.size.width / 6)
                    
                    Text("Gamer Central")
                        .multilineTextAlignment(.leading)
                        .frame(maxWidth: .infinity)
                        .font(.system(size: 36))
                        .foregroundColor(.red)
                        .fontWeight(.bold)
                }
                
                HStack {
                    Image(systemName: "lock")
                        .frame(width: gameGeometry.size.width / 6)
                    
                    Text("Gamer Central")
                        .multilineTextAlignment(.leading)
                        .frame(maxWidth: .infinity)
                        .font(.system(size: 36))
                        .foregroundColor(.red)
                        .fontWeight(.bold)
                }
                
                Spacer()
                
            }
        }
        .background(Color.gray)
    }
}

struct GCLoginView_Previews: PreviewProvider {
    static var previews: some View {
        GCLoginView()
    }
}
