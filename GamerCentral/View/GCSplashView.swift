//
//  GCSplashView.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/27/23.
//

import SwiftUI

struct GCSplashView: View {
    @State var isGlowing = false

    var body: some View {
        
        GeometryReader { gameGeometry in
            VStack(alignment: .center) {
                Spacer()

                Image(systemName: "gamecontroller.fill")
                    .resizable()
                    .frame(width: gameGeometry.size.width /  3, height: gameGeometry.size.height / 3)
                    .scaledToFit()
                    .foregroundColor(.black)
                    .overlay(
                        Image(systemName: "gamecontroller.fill")
                            .foregroundColor(.red)
                            .font(.system(size: 120))
                            .opacity(isGlowing ? 1 : 0)
                            .shadow(color: .yellow, radius: 10, x: 0, y: 0)
//                            .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true), value: isGlowing)
                    )
                    .onAppear{
                        isGlowing = true
                    }
                
                Text("Gamer Central")
                    .frame(maxWidth: .infinity)
                    .foregroundColor(.black)
                    .font(.system(size: 48))
                    .fontWeight(.bold)
                    .multilineTextAlignment(.center)
                    .overlay(
                        Text("Gamer Central")
                            .foregroundColor(.red)
                            .font(.system(size: 48))
                            .opacity(isGlowing ? 1 : 0)
                            .shadow(color: .yellow, radius: 10, x: 0, y: 0)
//                            .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true), value: isGlowing)
                    )
                    .onAppear{
                        isGlowing = true
                    }
                Spacer()
                Spacer()
            }
        }
        .background(.black)
    }
}

struct GCSplashView_Previews: PreviewProvider {
    static var previews: some View {
        GCSplashView()
    }
}
