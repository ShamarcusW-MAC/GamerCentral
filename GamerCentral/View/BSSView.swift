//
//  BSSView.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/27/23.
//

import SwiftUI
import Kingfisher

struct BSSView: View {
    
    @ObservedObject var platformViewModel: PlatformViewModel
    
    @FocusState private var readyToType: Bool

    @State var isShowingGameInfoPage = false

    var body: some View {
        
        GeometryReader { gameGeometry in
            VStack {
                
                Image(systemName: "chevron.up")
                    .font(.system(size: 24))
                    .foregroundColor(.red)
                
                HStack {
                    Image(systemName: "gamecontroller")
                        .font(.system(size: 28))
                        .foregroundColor(.red)
                    
                    SuperTextField(
                        placeholder:
                            Text("Search for any game...")
                            .font(.system(size: 18))
                            .foregroundColor(.red),
                        text: $platformViewModel.searchGameText)
                    .focused($readyToType)
                    .background(Capsule().foregroundColor(.gray.opacity(0.5)))
                    .font(.system(size: 28))
                    
                    
                    Button {
                        //function to fetch list based on search word
                        platformViewModel.fetchSearchGames()
                    } label: {
                        Image(systemName: "magnifyingglass")
                            .font(.system(size: 28))
                            .foregroundColor(.red)
                    }

                    
                }
                .padding(.top, 8)
                .padding(.bottom, 8)
                
                
                ScrollView {
                    LazyVStack(alignment: .leading) {
                        ForEach(platformViewModel.searchGames, id: \.self) { filteredGame in
                            if platformViewModel.state == .fetching {
                                ProgressView().foregroundColor(Color.white)
                            }
                            if platformViewModel.state == .done {
                                
                                Button {
                                    platformViewModel.fetchGameInfo(id: filteredGame.id, filteredGame: filteredGame)
                                    self.isShowingGameInfoPage.toggle()
                                } label: {
                                    HStack {
                                        
                                        KFImage(URL(string: filteredGame.backgroundImage ?? "" ))
                                            .placeholder {
                                                ProgressView().background(.white)
                                            }
                                            .resizable()
                                            .frame(width: gameGeometry.size.width / 4, height: gameGeometry.size.height / 8)
                                            .font(.system(size: 24))
                                            .scaledToFit()
                                            .clipShape(RoundedRectangle(cornerRadius: 16))
                                            .padding(8)
                                        
                                        Text(filteredGame.name)
                                            .foregroundColor(.red)
                                            .font(.system(size: 24))
                                        
                                    }
                                }
                            }
                        }
                        .animation(.easeInOut(duration: 0.3), value: platformViewModel.searchGames.count)
                    }
                }
            }
        }
        .background(.black)
        .sheet(isPresented: $isShowingGameInfoPage) {
            GameInfoView(platformViewModel: platformViewModel)
        }
//        .navigationDestination(isPresented: $isShowingGameInfoPage) {
//            GameInfoView(platformViewModel: platformViewModel)
//        }

    }
}

struct BSSView_Previews: PreviewProvider {
    static var previews: some View {
        BSSView(platformViewModel: PlatformViewModel.mock())
    }
}

struct SuperTextField: View {
    
    var placeholder: Text
    @Binding var text: String
    var editingChanged: (Bool)->() = { _ in }
    var commit: ()->() = { }
    
    var body: some View {
        ZStack(alignment: .leading) {
            if text.isEmpty { placeholder
                .padding(.leading, 8)
            }
            TextField("", text: $text, onEditingChanged: editingChanged, onCommit: commit)
                .padding(.leading, 8)
                .foregroundColor(.red)
        }
    }
    
}


//struct PBTTexInput: View {
//    
//}
