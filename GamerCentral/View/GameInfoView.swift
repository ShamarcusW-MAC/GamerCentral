//
//  GameDetailView.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/26/23.
//

import SwiftUI
import Kingfisher

struct GameInfoView: View {
    
    @ObservedObject var platformViewModel: PlatformViewModel
    @State private var isZoomedIn = false
    @State private var currentIndex = 0
    
    var body: some View {
        
        GeometryReader { gameGeometry in
            
            VStack(spacing: 0) {
                KFImage(URL(string: platformViewModel.gameInfo?.backgroundImage ?? ""))
                    .placeholder {
                        //                                            ProgressView()
                        //                                                .foregroundColor(.white)
                        Image(systemName: "gamecontroller")
                            .resizable()
                            .frame(width: gameGeometry.size.width / 3, height: gameGeometry.size.height / 5 )
                    }
                    .resizable()
                    .frame(width: gameGeometry.size.width, height: gameGeometry.size.height / 3)
                    .scaledToFit()
                    .padding(.bottom, 8)
                
                ScrollView {
                    
                    VStack(spacing: 16) {
                        
                        Text(platformViewModel.gameInfo?.name ?? "No title!")
                            .multilineTextAlignment(.leading)
                            .foregroundColor(.red)
                            .font(.system(size: 24))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                        
                        Text(platformViewModel.gameInfo?.description.cleanHTML() ?? "No description!")
                            .multilineTextAlignment(.leading)
                            .foregroundColor(.red)
                            .font(.system(size: 12))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                        
                        HStack(spacing: 8) {
                            Text("Release Date: ")
                                .foregroundColor(.white)
                                .font(.system(size: 14))
                            
                            Text(platformViewModel.gameInfo?.released ?? "N/A")
                                .multilineTextAlignment(.leading)
                                .foregroundColor(.red)
                                .font(.system(size: 14))
                                .fontWeight(.semibold)
                                .frame(maxWidth: .infinity, alignment: .leading)
                        }
                        
                        HStack(spacing: 8) {
                            Text("Developer: ")
                                .foregroundColor(.white)
                                .font(.system(size: 14))
                            
                            if let developers = platformViewModel.gameInfo?.developers, !developers.isEmpty {
                                
                                ForEach(developers, id: \.self) { developer in
                                    Text(developer.name)
                                        .padding(.horizontal, 8)
                                        .padding(.vertical, 4)
                                        .clipShape(Capsule())
                                        .background(.gray)
                                        .multilineTextAlignment(.leading)
                                        .foregroundColor(.red)
                                        .font(.system(size: 10))
                                        .fontWeight(.semibold)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                }
                            } else {
                                Text("N/A")
                                    .foregroundColor(.red)
                                    .multilineTextAlignment(.leading)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                            }
                            
                        }

                        HStack(spacing: 8) {
                            Text("Publisher: ")
                                .foregroundColor(.white)
                                .font(.system(size: 14))
                            
                            if let publishers = platformViewModel.gameInfo?.publishers, !publishers.isEmpty {
                                ForEach(publishers, id: \.self) { publisher in
                                    Text(publisher.name)
                                        .padding(.horizontal, 8)
                                        .padding(.vertical, 4)
                                        .foregroundColor(.red)
                                        .font(.system(size: 10))
                                        .fontWeight(.semibold)
                                        .clipShape(Capsule())
                                        .background(.gray)
                                        .multilineTextAlignment(.leading)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                    
                                }
                                
                            } else {
                                Text("N/A")
                                    .foregroundColor(.red)
                                    .multilineTextAlignment(.leading)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                            }
                            
                            
                        }
                        
                        HStack(spacing: 8) {
                            Text("Website: ")
                                .foregroundColor(.white)
                                .font(.system(size: 14))
                            
                            if let website = platformViewModel.gameInfo?.website, !website.isEmpty {
                                Link(website, destination: URL(string: website)!)
                                    .multilineTextAlignment(.leading)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                            } else {
                                Text("No link at the moment! :(")
                                    .foregroundColor(.red)
                                    .multilineTextAlignment(.leading)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                            }

                        }
                        
                        
                        Text("Screenshots")
                            .foregroundColor(.red)
                            .font(.system(size: 32))
                            .frame(maxWidth: .infinity, alignment: .center)
                        
                            
                            ZStack {
                                ScrollView(.horizontal) {
                                    LazyHStack {
                                        ForEach(platformViewModel.screenshots, id: \.self) { screenshot in
                                            
                                            KFImage(URL(string: platformViewModel.screenshots[currentIndex].image))
                                                .placeholder {
                                                    Image(systemName: "gamecontroller")
                                                        .resizable()
                                                        .frame(width: gameGeometry.size.width, height: gameGeometry.size.height / 5)
                                                }
                                                .resizable()
                                                .frame(width: gameGeometry.size.width, height: gameGeometry.size.height / 3)
                                                .scaledToFit()
                                                .padding(.bottom, 8)
                                                .onTapGesture {
                                                    withAnimation {
                                                        isZoomedIn = true
                                                    }
                                                }
                                                .gesture(
                                                    DragGesture()
                                                        .onChanged({ value in
                                                            if value.translation.height > 100 {
                                                                isZoomedIn = false
                                                            }
                                                        })
                                                )
                                                .fullScreenCover(isPresented: $isZoomedIn) {
                                                    FocusedImage(urlString: platformViewModel.screenshots[currentIndex].image, isZoomedIn: $isZoomedIn)
                                                }
                                        }
                                    }
                                }
                                .scrollDisabled(true)
                                
                                HStack {
                                    Button(action: {
                                        // handle left navigation
                                        if currentIndex > 0 {
                                            currentIndex -= 1
                                        }
                                    }, label: {
                                        Image(systemName: "chevron.left")
                                            .foregroundColor(.white)
                                            .padding()
                                            .background(Color.red.opacity(0.5))
                                            .clipShape(Rectangle())
                                        
                                    })
                                    .disabled(currentIndex == 0 ? true : false)
                                    .opacity(currentIndex == 0 ? 0 : 0.8)
                                    
                                    Spacer()
                                    
                                    Button(action: {
                                        // handle right navigation
                                        
                                        if currentIndex < platformViewModel.screenshots.count - 1 {
                                            currentIndex += 1

                                        }
                                    }, label: {
                                        Image(systemName: "chevron.right")
                                            .foregroundColor(.white)
                                            .padding()
                                            .background(Color.red.opacity(0.5))
                                            .clipShape(Rectangle())
                                    })
                                    .disabled(currentIndex == platformViewModel.screenshots.count - 1 ? true : false)
                                    .opacity(currentIndex == platformViewModel.screenshots.count - 1 ? 0 : 0.8)
                                    
                                }
                            }
                            
                    }
                }
            }
        }
        .background(.black)
    }
    
}


struct GameInfoView_Previews: PreviewProvider {
    static var previews: some View {
        GameInfoView(platformViewModel: PlatformViewModel.mock())
    }
}

extension String {
    func cleanHTML() -> String {
        return self
        .replacingOccurrences(of: "&#39;", with: "'")
//        .replacingOccurrences(of: "<", with: "*")
//        .replacingOccurrences(of: ">", with: "*")
//        .replacingOccurrences(of: "/", with: "*")
//        .replacingOccurrences(of: "h3", with: "")
//        .replacingOccurrences(of: "/h3", with: "")
//        .replacingOccurrences(of: "<h3>", with: "")
//        .replacingOccurrences(of: "</h3", with: "")
//        .removeBetween(char1: "*", char2: "*", letter: "p")
    }
}

extension String {
    func removeBetween(char1: Character, char2: Character, letter: Character) -> String {
        let pattern = "\(char1)\(letter)\(char2)"
        return self.replacingOccurrences(of: pattern, with: "")
    }
}

struct FocusedImage: View {
    var urlString: String
    @Binding var isZoomedIn: Bool
    var orientation: UIDeviceOrientation {
        UIDevice.current.orientation
    }
    var body: some View {
        GeometryReader { imageGeometry in
            VStack {
                HStack {
                    Button {
                        //Dimiss the view
                        isZoomedIn = false
                    } label: {
                        Image(systemName: "xmark")
                            .foregroundColor(.red)
                            .font(.system(size: 24))
                    }
                    
                    Spacer()
                }
                
                Spacer()
                
                KFImage(URL(string: urlString))
                    .resizable()
                    .frame(width: imageGeometry.size.width, height: orientation.isLandscape ? imageGeometry.size.height : imageGeometry.size.height / 3)
                
                Spacer()
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(.black)
            
        }
    }
}
