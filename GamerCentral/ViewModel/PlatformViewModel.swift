//
//  PlatformViewModel.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/23/23.
//

import Foundation
import SwiftUI

class PlatformViewModel: ObservableObject {
    let app: GamerAppController
    let manager: GamingNetworkManager
    
    @Published private(set) var state: State? = .idle

    //For video game platforms
    @Published var platforms = [Platform]()
    @Published var chosenPlatform: Platform?
    
    //For video game genres
    @Published var genres = [Platform]()
    @Published var chosenGenre: Platform?
    
    
    //For video game platforms
    @Published var filteredGames = [GameDetail]()
    @Published var screenshots = [Screenshot]()
    
    //Video game information
    @Published var gameInfo: GameInfo?
    
    @Published var searchGameText = ""
    @Published var page = 1
    @Published var searchGames = [GameDetail]()
//    var entireGameFilter: [GameDetail] {
//        return searchGameText == "" ? allGames : allGames.filter {
//            $0.name.lowercased().starts(with: searchGameText.lowercased())
//        }
//    }
    
    @Published var navigationPath = NavigationPath()
    
    static func mock() -> PlatformViewModel {
        return GamerAppController().platformViewModel
    }
    
    init(app: GamerAppController, manager: GamingNetworkManager) {
        self.app = app
        self.manager = manager
        KeyRef.checkIfKeychainItemExists()
        self.fetchPlatforms()
    }
    
//    deinit () {
//        print("Platform view model destroyed!")
//    }

    
    func fetchPlatforms() {
        Task {
            do {
                state = .fetching
                print("Fetching platforms...")
                let _platforms = try await manager.fetchData(endpoint: Endpoints.gamerCentralPlatforms(), useCache: true)

                await MainActor.run {
                    state = .done
                    platforms = _platforms.results
                }
            } catch let error {
                state = .error
                print("Error fetching data: \(error)")
            }

        }
    }

    
//    func fetchPlatforms() {
//        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
//            guard let self = self else { return }
//            do {
//                print("Fetching platforms...")
//                let _platforms = try await manager.fetchData(endpoint: Endpoints.gamerCentralPlatforms(), useCache: true)
//
//                DispatchQueue.main.async {
//                    self.platforms = _platforms.results
//                }
//            } catch let error {
//                print("Error fetching data: \(error)")
//            }
//        }
//    }
//
    
    func fetchPlatformDetail(result: Platform) {
        self.chosenPlatform = result
    }
    
    func fetchGenres() {
        Task {
            do {

                let _genres = try await manager.fetchData(endpoint: Endpoints.gamerCentralGenres(), useCache: true)
                
                await MainActor.run {
                    state = .fetching
                    genres = _genres.results
                    state = .done

                }

                print("Genres: \(genres)")
            } catch let error {
                state = .error
                print("Error fetching data: \(error)")
            }
        }
    }
    
    func fetchGenreDetail(genre: Platform){
        self.chosenGenre = genre
    }
    
    func fetchSearchGames() {
        Task {
            do {

                let _searchGames = try await manager.fetchData(endpoint: Endpoints.gamerCentralSearchGames(searchWord: searchGameText.replacingOccurrences(of: " ", with: "+"), page: page), useCache: true)

                await MainActor.run {
                    state = .fetching
                    searchGames = _searchGames.results
                    state = .done

                }
                
            } catch let error {
                await MainActor.run {
                    
                    state = .error
                    print("Error fetching data: \(error)")
                }
            }
        }
    }
    
    func fetchPlatformAndGenreGames(platformId: Int, genreId: Int) {
        Task {
            do {

                let _games = try await manager.fetchData(endpoint: Endpoints.gamerCentralPlatformAndGenreGames(platformId: platformId, genreId: genreId), useCache: true)
                
                await MainActor.run {
//                    games = _games.results
//
//                    filteredGames = games.filter {
//                        $0.genres.contains(where: { genre in
//                            genre.name == chosenGenre?.name
//                        })
//                    }

                    state = .fetching
                    filteredGames = _games.results
                    state = .done
                    
                }
                print("Platform Id: \(platformId)")
                print("Genre Id: \(genreId)")
            } catch let error {
                await MainActor.run {
                    
                    state = .error
                    print("Error fetching data: \(error)")
                }
            }
        }
    }
    
    
    func fetchGameInfo(id: Int, filteredGame: GameDetail) {
        Task {
            do {
                let _gameInfo = try await manager.fetchData(endpoint: Endpoints.gamerCentralGameDetail(id: id), useCache: true)
                
                await MainActor.run {
                    state = .fetching
                    gameInfo = _gameInfo
                    screenshots = filteredGame.shortScreenshots
                    state = .done
                }

            } catch let error {
                await MainActor.run {
                    state = .error
                    print("Error fetching game info: \(error)")
                }
            }
        }
    }
}

extension PlatformViewModel {
    enum State {
        case idle
        case fetching
        case done
        case error
    }
}

private func getViewController() -> UIViewController {
    return (UIApplication.shared.windows.first?.rootViewController)!
}
