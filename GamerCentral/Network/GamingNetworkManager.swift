//
//  GamingNetworkManager.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/23/23.
//

import Foundation

class GamingNetworkManager {
    
    lazy var jsonDecoder: JSONDecoder = {
        JSONDecoder()
    }()
    
    // Create a shared instance of URLCache
    static let sharedCache: URLCache = {
        let cacheSize = 50 * 1024 * 1024 // 50 MB
        let cache = URLCache(memoryCapacity: cacheSize, diskCapacity: cacheSize)
        return cache
    }()
    
//    func fetchData<Response>(endpoint: GCEndpoint<Response>, useCache: Bool = false, configuration: URLSessionConfiguration = .default) async throws -> Response {
//        // Use the sharedCache when creating the URLSessionConfiguration
////        let configuration = URLSessionConfiguration.default
////        configuration.urlCache = GamingNetworkManager.sharedCache
////        configuration.requestCachePolicy = .returnCacheDataElseLoad // Return cached data if available, otherwise load from network
//
//        var request = URLRequest(url: endpoint.url)
//        request.httpMethod = "GET"
//
//        if useCache {
//            request.cachePolicy = .returnCacheDataElseLoad
//        } else {
//            request.cachePolicy = .reloadIgnoringLocalCacheData
//
//        }
//
//        let (data, response) = try await URLSession(configuration: configuration).data(for: request)
//
//        guard let httpResponse = response as? HTTPURLResponse else {
//            throw URLError(.badServerResponse)
//        }
//
//        guard(200...299).contains(httpResponse.statusCode) else {
//            throw URLError(.badServerResponse)
//        }
//
//        print("Gaming Data: \(data)")
//        return try jsonDecoder.decode(Response.self, from: data)
//    }
    
    
    func fetchDataCacheWithCustomConfig<Response>(endpoint: GCEndpoint<Response>, useCache: Bool = false, configuration: URLSessionConfiguration = .default) async throws -> Response {
        // Use the sharedCache when creating the URLSessionConfiguration
//        let configuration = URLSessionConfiguration.default
//        configuration.urlCache = GamingNetworkManager.sharedCache
//        configuration.requestCachePolicy = .returnCacheDataElseLoad // Return cached data if available, otherwise load from network
        
        var request = URLRequest(url: endpoint.url)
        request.httpMethod = "GET"
        
        if useCache {
            request.cachePolicy = .returnCacheDataElseLoad
        } else {
            request.cachePolicy = .reloadIgnoringLocalCacheData

        }
        
        let (data, response) = try await URLSession(configuration: configuration).data(for: request)
        
        guard let httpResponse = response as? HTTPURLResponse else {
            throw URLError(.badServerResponse)
        }
        
        guard(200...299).contains(httpResponse.statusCode) else {
            throw URLError(.badServerResponse)
        }
        
        print("Gaming Data: \(data)")
        return try jsonDecoder.decode(Response.self, from: data)
    }
    
    
    func fetchData<Response>(endpoint: GCEndpoint<Response>, useCache: Bool = false) async throws -> Response {
        if useCache, let cachedResponse = URLCache.shared.cachedResponse(for: URLRequest(url: endpoint.url)) {
            print("Fetched cached response for \(endpoint.url)")
            return try jsonDecoder.decode(Response.self, from: cachedResponse.data)
        }
        
        
        if let cachedResponse = GamingNetworkManager.sharedCache.cachedResponse(for: URLRequest(url: endpoint.url)) {
            print("Cached response: \(cachedResponse)")
        } else {
            print("No cached response found.")
        }
        
        let (data, response) = try await URLSession.shared.data(from: endpoint.url)
        
        guard let httpResponse = response as? HTTPURLResponse else {
            throw URLError(.badServerResponse)
        }
        
        guard(200...299).contains(httpResponse.statusCode) else {
            throw URLError(.badServerResponse)
        }
        
        if useCache {
            let cachedResponse = CachedURLResponse(response: httpResponse, data: data)
            URLCache.shared.storeCachedResponse(cachedResponse, for: URLRequest(url: endpoint.url))
            print("Stored response in cache for \(endpoint.url)")
        }
        
        print("Fetched response from network for \(endpoint.url)")
        return try jsonDecoder.decode(Response.self, from: data)
    }
}

