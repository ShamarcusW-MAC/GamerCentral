//
//  Endpoints.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/23/23.
//

import Foundation
import Security

struct GCEndpoint<Response: Decodable> {
    let url: URL
    let responseType: Response.Type
}

enum GCEndpointError: Error {
    case invalidParameter(String)
    case networkError(Error)
    case invalidResponse
    case decodingError(Error)
}

class Endpoints {

    static func gamerCentralPlatforms() -> GCEndpoint<Overview>{
        GCEndpoint(url: URL(string: "https://api.rawg.io/api/platforms?key=\(KeyRef.getAPIKeyThroughConfig())")!,
                   responseType: Overview.self)
    }

    static func gamerCentralGenres() -> GCEndpoint<Overview>{
        GCEndpoint(url: URL(string: "https://api.rawg.io/api/genres?key=\(KeyRef.getAPIKeyThroughConfig())")!, responseType: Overview.self)
    }

    static func gamerCentralSearchGames(searchWord: String, page: Int) -> GCEndpoint<Games>{
        GCEndpoint(url: URL(string:"https://api.rawg.io/api/games?key=\(KeyRef.getAPIKeyThroughConfig())&page=\(page)&search=\(searchWord)")!, responseType: Games.self)
    }
    
//    static func gamerCentralSearchGames(searchWord: String, page: Int, completion: @escaping (Result<Games, Error>) -> Void) -> GCEndpoint<Games> {
//        guard !searchWord.isEmpty else {
//            completion(.failure(GCEndpointError.invalidParameter("searchWord")))
//            return
//        }
//        guard page > 0 else {
//            completion(.failure(GCEndpointError.invalidParameter("page")))
//            return
//        }
//
//        var urlComponents = URLComponents(string: "https://api.rawg.io/api/games")!
//        var queryItems: [URLQueryItem] = [URLQueryItem(name: "key", value: KeyRef.getAPIKeyThroughConfig()), URLQueryItem(name: "page", value: String(page)), URLQueryItem(name: "search", value: searchWord.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))    ]
//        urlComponents.queryItems = queryItems
//
//        let endpoint = GCEndpoint(url: urlComponents.url!, responseType: Games.self)
//        endpoint.execute() { (result: Result<Games, Error>) in
//            switch result {
//            case .success(let games):
//                completion(.success(games))
//            case .failure(let error):
//                completion(.failure(error))
//            }
//        }
//    }
//    In this updated version, the function now takes a completion handler that returns a Result object with the Games data or an error. The function first validates the input parameters and returns an error if they are invalid. It then constructs the URL for the API request using URLComponents to ensure that the searchWord parameter is properly encoded. The function then creates an instance of GCEndpoint and executes it, passing the results to the completion handler.






        static func gamerCentralPlatformAndGenreGames(platformId: Int, genreId: Int) -> GCEndpoint<Games>{
            GCEndpoint(url: URL(string:"https://api.rawg.io/api/games?key=\(KeyRef.getAPIKeyThroughConfig())&platforms=\(platformId)&genres=\(genreId)")!, responseType: Games.self)
        }

    static func gamerCentralGameDetail(id: Int) -> GCEndpoint<GameInfo>{
        GCEndpoint(url: URL(string:"https://api.rawg.io/api/games/\(id)?key=\(KeyRef.getAPIKeyThroughConfig())")!, responseType: GameInfo.self)
    }
}

//https://api.rawg.io/api/games?key=cc67ed0c23074756a448c851ff4ca95a&page=2&search=portal
