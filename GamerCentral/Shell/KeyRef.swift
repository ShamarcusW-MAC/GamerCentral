//
//  KeyRef.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/28/23.
//

import Foundation
import Security

class KeyRef {
    //Get API Key through environment variable
    static let apiKey = ProcessInfo.processInfo.environment["API_KEY"]
    
    static let keyChainItemName = "com.shamarcus.walker.GamerCentral.apikey"
    
    static let keyChainQuery: [NSString: AnyObject] = [
        kSecClass: kSecClassGenericPassword,
        kSecAttrAccount: keyChainItemName as AnyObject,
        kSecAttrService: keyChainItemName as AnyObject,
        kSecAttrAccessible: kSecAttrAccessibleAfterFirstUnlock,
        kSecMatchLimit: kSecMatchLimitOne,
        kSecReturnData: true as AnyObject
    ]
    
    static func storeApiKeyinKeyChain(){
        // Storing api key within keychain
        
        let access = SecAccessControlCreateWithFlags(nil, kSecAttrAccessibleAfterFirstUnlock, .userPresence, nil)
        
        guard let apiKeyData = getAPIKeyThroughConfig().data(using: .utf8) else {
            print("Error converting API key to data.")
            return
        }

        let keyChainQuery: [NSString: AnyObject] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: keyChainItemName as AnyObject,
            kSecAttrService: keyChainItemName as AnyObject,
            kSecAttrAccessible: kSecAttrAccessibleAfterFirstUnlock,
            kSecValueData: apiKeyData as AnyObject,
            kSecAttrLabel: "API Key" as AnyObject,
        ]

        let status = SecItemAdd([
            kSecValueData: keyChainQuery,
            kSecAttrAccessControl: access as AnyObject,
            kSecAttrDescription: "Gamer Central's API Key"
        ] as CFDictionary, nil)

        if status == errSecSuccess {
            print("API Key stored successfully in keychain!")
        } else {
            print("Error storing API key in keychain: \(status)")
        }
    }
    
    
    static func retrieveApiKeyInKeyChain() -> String {
        var apiKey: String = ""
        
        //Retrieving the API Key
//        let keyChainQuery: [NSString: AnyObject] = [
//            kSecClass: kSecClassGenericPassword,
//            kSecAttrAccount: keyChainItemName as AnyObject,
//            kSecAttrService: keyChainItemName as AnyObject,
//            kSecAttrAccessible: kSecAttrAccessibleAfterFirstUnlock,
//            kSecMatchLimit: kSecMatchLimitOne,
//            kSecReturnData: true as AnyObject
//        ]
        var item: AnyObject?
        let status = SecItemCopyMatching(keyChainQuery as CFDictionary, &item)
        
        if status == errSecSuccess
        {
            // Convert the retrieved data to a string
            if let apiKeyData = item as? Data {
                apiKey = String(data: apiKeyData, encoding: .utf8) ?? ""
                print("Retrieved API key: \(apiKey)")
            } else {
                print("Error converting API key data to string.")
            }
        } else {
            print("Error retrieving API key from keychain: \(status)")
        }
        
        return apiKey
    }
    
    static func deleteAPIKeyChainItem() {
//      Deleting a keychain item
        
//      Set the keychain query attributes to retrieve the keychain item
        let keychainQuery: [NSString: AnyObject] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: keyChainItemName as AnyObject,
            kSecAttrService: keyChainItemName as AnyObject,
            kSecMatchLimit: kSecMatchLimitOne,
            kSecReturnData: kCFBooleanTrue
        ]

        // Retrieve the keychain item
        var item: AnyObject?
        let status = SecItemCopyMatching(keychainQuery as CFDictionary, &item)

        if status == errSecSuccess {
            // Delete the keychain item
            let deleteQuery: [NSString: AnyObject] = [
                kSecClass: kSecClassGenericPassword,
                kSecAttrAccount: keyChainItemName as AnyObject,
                kSecAttrService: keyChainItemName as AnyObject,
            ]

            let deleteStatus = SecItemDelete(deleteQuery as CFDictionary)

            if deleteStatus == errSecSuccess {
                print("API key deleted from keychain.")
            } else {
                print("Error deleting API key from keychain: \(deleteStatus)")
            }
        } else {
            print("Error retrieving API key from keychain: \(status)")
        }
    }
    
    
    static func getAPIKeyThroughConfig() -> String {
        guard let path = Bundle.main.path(forResource: "Config", ofType: "plist") else {
            fatalError("Could not find Config.plist")
        }

        let url = URL(fileURLWithPath: path)
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Could not find Config.plist")
        }

        guard let dictionary = try? PropertyListSerialization.propertyList(from: data, format: nil) as? [String : Any] else {
            fatalError("Could not find Config.plist")
        }

        guard let apiKey = dictionary["API_KEY"] as? String else {
            fatalError("Could not find API_KEY in Config.plist")
        }

        return apiKey
    }
    
    
    static func checkIfKeychainItemExists() {
        
        //Checking if keychain item exists
        var item: AnyObject?
        let status = SecItemCopyMatching(keyChainQuery as CFDictionary, &item)

        if status == errSecSuccess {
            // Keychain item with the same attributes already exists
            print("Keychain item already exists: \(item as AnyObject)")
        } else if status == errSecItemNotFound {
            // Keychain item with the same attributes does not exist
            print("Keychain item not found.")
        } else {
            // Other error
            print("Error checking keychain: \(status)")
        }
    }
}
