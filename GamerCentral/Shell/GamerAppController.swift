//
//  GamerAppController.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/23/23.
//

import Foundation

class GamerAppController {
    
    lazy var gamingNetworkManager: GamingNetworkManager = {
        GamingNetworkManager()
    }()
    
    static func mockk() -> GamerAppController {
        GamerAppController()
    }
    
    lazy var platformViewModel: PlatformViewModel = {
        PlatformViewModel(app: self, manager: gamingNetworkManager)
    }()
}
