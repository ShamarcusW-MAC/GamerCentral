//
//  GamerCentralApp.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/23/23.
//

import SwiftUI

@main
struct GamerCentralApp: App {
    
    let app = GamerAppController()
    var body: some Scene {
        WindowGroup {
            
            GCSplashView()
                .onAppear{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        let rootView =             RootNavigationView(platformViewModel: app.platformViewModel)
                        let hostingController = UIHostingController(rootView: rootView)
                        
                        hostingController.modalPresentationStyle = .fullScreen
                        if let windowScene = UIApplication.shared.windows.first?.windowScene {
                                         let windows = windowScene.windows
                                         if !windows.isEmpty {
                                             windows[0].rootViewController = hostingController
                                         }
                                     }
                    }
                }
            
        }
    }
}
