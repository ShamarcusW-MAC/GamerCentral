//
//  RootNavigationView.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/23/23.
//

import SwiftUI

struct  RootNavigationView: View {
    
    @ObservedObject var platformViewModel: PlatformViewModel
    
    var body: some View {
        NavigationStack(path: $platformViewModel.navigationPath) {
            HomeView(platformViewModel: platformViewModel)
        }
    }
}
