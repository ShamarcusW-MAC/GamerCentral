//
//  Overview.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/23/23.
//

import Foundation

struct Overview: Decodable {
    let count: Int
    let next: String?
    let previous: String?
    let results: [Platform]
}

struct Platform: Decodable, Identifiable, Hashable {
    let id: Int
    let name: String
    let imageBackground: String
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case imageBackground = "image_background"
    }
}

extension Platform {
    static func == (lhs: Platform, rhs: Platform) -> Bool {
        lhs.id == rhs.id
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
