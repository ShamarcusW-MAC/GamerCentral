//
//  GameInfo.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/26/23.
//

import Foundation

struct GameInfo: Decodable {
    let id: Int
    let name: String
    let description: String
    let released: String?
    let backgroundImage: String?
    let backgroundImageAdditional: String?
    let website: String
    let developers: [Developer]
    let publishers: [Developer]
    
    enum CodingKeys: String, CodingKey {
        case id, name, description, released
        case backgroundImage = "background_image"
        case backgroundImageAdditional = "background_image_additional"
        case website
        case developers, publishers
    }
}

struct Developer: Decodable, Hashable {
    let id: Int
    let name: String
}

extension Developer {
    static func == (lhs: Developer, rhs: Developer) -> Bool {
        lhs.id == rhs.id
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
