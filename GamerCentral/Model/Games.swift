//
//  Games.swift
//  GamerCentral
//
//  Created by Sha'Marcus Walker on 2/26/23.
//

import Foundation

struct Games: Decodable {
    let count: Int
    let next: String?
    let previous: String?
    let results: [GameDetail]
}

struct GameDetail: Decodable, Identifiable, Hashable {
    let id: Int
    let name: String
    let platforms: [GamePlatform]
    let backgroundImage: String?
    let rating: Double
    let shortScreenshots: [Screenshot]
    let genres: [Genre]
    
    enum CodingKeys: String, CodingKey {
        case id, name, platforms
        case backgroundImage = "background_image"
        case rating
        case shortScreenshots = "short_screenshots"
        case genres
    }
}

struct GamePlatform: Decodable {
    let platform: Genre
}

struct Genre: Decodable, Equatable {
    let id: Int
    let name: String
}

struct Screenshot: Decodable, Hashable {
    let id: Int
    let image: String
}

extension GameDetail {
    static func == (lhs: GameDetail, rhs: GameDetail) -> Bool {
        lhs.id == rhs.id
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

extension Screenshot {
    static func == (lhs: Screenshot, rhs: Screenshot) -> Bool {
        lhs.id == rhs.id
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
